<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ page import="main.java.login.entity.*" %>
<%
 String host = request.getHeader("host");
 String path = "http://" + host + request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>登录后展示</title>
</head>
<body>
	<center>
		<c:if test="${!empty user}">
			<table>
				<tr><td>用户编码</td><td><c:out value="${user.id}"></c:out></td></tr>
				<tr><td>用户名</td><td><c:out value="${user.username}"></c:out></td></tr>
				<tr><td>是否锁定</td><td><c:out value="${user.locked}"></c:out></td></tr>
			</table>
			<shiro:hasPermission name="user:list">
				<a href="toLookUsers">查看用户列表</a>
			</shiro:hasPermission>
		</c:if>
		<c:if test="${empty user}">没有此用户</c:if>

			<a href="toLogout">退出</a>
	</center>
	
</body>
</html>