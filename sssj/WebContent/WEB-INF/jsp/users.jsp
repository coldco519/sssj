<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ page import="main.java.login.entity.*" %>
<%
 String host = request.getHeader("host");
 String path = "http://" + host + request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<%-- 一定要注意顺序 --%>
<script type="text/javascript" src="<%=path %>/resources/js/highcharts/jquery-1.8.3.js"></script>
<script type="text/javascript" src="<%=path %>/resources/js/highcharts/highcharts.js"></script>

<%--打印、导出 --%>
<script src="<%=path %>/resources/js/highcharts/modules/exporting.js"></script>

<script type="text/javascript" src="<%=path %>/resources/js/user_charts.js"></script>
<title>登录后展示</title>
</head>
<body>
	<center>
		<c:if test="${!empty users}">
			<table border="1px solid red">
				<thead>
					<tr>
						<td>用户编码</td>
						<td>用户名</td>
						<td>是否锁定</td>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${users }" var="user">
						<tr>
							<td><c:out value="${user.id }"></c:out></td>
							<td><c:out value="${user.username }"></c:out></td>
							<td><c:out value="${user.locked }"></c:out></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:if>
		<c:if test="${empty users}">没有此用户</c:if>
	
	<%-- --%>
	<shiro:hasPermission name="user:print"> 
	<%--<shiro:hasRole name="admin"> </shiro:hasRole>--%>
			 	<input type="button" value="打印" onclick="print()"/> 
	
		<%-- 	--%>
		</shiro:hasPermission> 
		<shiro:hasRole name="admin">
			<input type="button" value="图表展示" id="showCharts" onclick="showCharts()"/>
		</shiro:hasRole>
		<a href="toLogout">退出</a>
	</center>
	<div id="container" style="min-width: 800px; height: 400px"></div>
</body>

<script type="text/javascript">
	function print(){
		window.open("toPrint");
	}
</script>
</html>