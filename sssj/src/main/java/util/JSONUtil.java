package main.java.util;


import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**   
* @Title: JSONUtil.java 
* @Description: JSON工具类
* @version V1.0   
*/ 
public class JSONUtil
{

	 private static Log log = LogFactory.getLog(JSONUtil.class);
	 private static ThreadLocal<StringBuilder> local = new ThreadLocal<StringBuilder>();

    // private static final Map<Class, BeanInfo> beanInfoMap = new
    // ConcurrentHashMap<Class, BeanInfo>();
    // 该用哪一种Map那？
    @SuppressWarnings({ "rawtypes" })
    private static Map<Class, BeanInfo> beanInfoMap = Collections.synchronizedMap(new WeakHashMap<Class, BeanInfo>());

    /** 
    * @Title: printJson 
    * @Description: 輸出函數
    * @param request
    * @param response
    * @param returnValue
    * @return
    * @throws Exception
    *        String 
    */ 
    public static String printJson(HttpServletRequest request, HttpServletResponse response, Object returnValue) throws Exception {
        String str = toJson(returnValue);
        response.setContentType("text/html; charset=utf-8");
        response.setCharacterEncoding("utf-8");
        response.getWriter().print(str);
        response.getWriter().close();
        log.debug("json-----------------------------------------------------:" + str);
        return null;
    }

    /** 
    * @Title: stringPrintJson 
    * @Description: 拼接好的String直接输入Json
    * @param request
    * @param response
    * @param str
    * @return
    * @throws Exception
    *        String 
    */ 
    public static String stringPrintJson(HttpServletRequest request, HttpServletResponse response, String str) throws Exception {
        response.setContentType("text/html; charset=utf-8");
        response.setCharacterEncoding("utf-8");
        response.getWriter().print(str);
        log.debug("json-----------------------------------------------------:" + str);
        return null;
    }

    /** 
    * @Title: toJson 
    * @Description: 通用轉換入口
    * @param obj
    * @return
    *        String 
    */ 
    private static String toJson(Object obj) {
        fromObject(obj);
        StringBuilder builder = builder();
        local.remove();

        return builder.toString();
    }

    private static StringBuilder builder() {
        StringBuilder builder = local.get();
        if (builder == null) {
            builder = new StringBuilder();
            local.set(builder);
        }
        return builder;
    }

    @SuppressWarnings("unchecked")
    private static void fromObject(Object obj) {
        if (obj == null) {
            builder().append("null");
        } else if (obj instanceof String) {
            builder().append(quote((String) obj));
        } else if (obj instanceof Number || obj instanceof Character || obj instanceof Boolean) {
            fromPrimitive(obj);
        } else if (obj instanceof Date) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date = format.format((Date) obj);
            builder().append("\"").append(date).append("\"");
        } else if (obj.getClass().isArray()) {
            fromArray(obj);
        } else if (obj instanceof Collection) {
            fromCollection((Collection<Object>) obj);
        } else if (obj instanceof Map) {
            fromMap((Map<Object, Object>) obj);
        } else {
            fromBean(obj);
        }
    }

    private static void fromPrimitive(Object obj) {
        if (obj instanceof Character) {
            Character c = (Character) obj;
            char[] carr = { c };
            builder().append(quote(new String(carr)));
        } else {
            builder().append(obj);
        }
    }

    // 该方法拷贝自net.sf.json.util.JSONUtils
    private static String quote(String string) {
        char b;
        char c = 0;
        int i;
        int len = string.length();
        StringBuffer sb = new StringBuffer(len * 2);
        String t;
        char[] chars = string.toCharArray();
        char[] buffer = new char[1030];
        int bufferIndex = 0;
        sb.append('"');
        for (i = 0; i < len; i += 1) {
            if (bufferIndex > 1024) {
                sb.append(buffer, 0, bufferIndex);
                bufferIndex = 0;
            }
            b = c;
            c = chars[i];
            switch (c) {
            case '\\':
            case '"':
                buffer[bufferIndex++] = '\\';
                buffer[bufferIndex++] = c;
                break;
            case '/':
                if (b == '<') {
                    buffer[bufferIndex++] = '\\';
                }
                buffer[bufferIndex++] = c;
                break;
            default:
                if (c < ' ') {
                    switch (c) {
                    case '\b':
                        buffer[bufferIndex++] = '\\';
                        buffer[bufferIndex++] = 'b';
                        break;
                    case '\t':
                        buffer[bufferIndex++] = '\\';
                        buffer[bufferIndex++] = 't';
                        break;
                    case '\n':
                        buffer[bufferIndex++] = '\\';
                        buffer[bufferIndex++] = 'n';
                        break;
                    case '\f':
                        buffer[bufferIndex++] = '\\';
                        buffer[bufferIndex++] = 'f';
                        break;
                    case '\r':
                        buffer[bufferIndex++] = '\\';
                        buffer[bufferIndex++] = 'r';
                        break;
                    default:
                        t = "000" + Integer.toHexString(c);
                        int tLength = t.length();
                        buffer[bufferIndex++] = '\\';
                        buffer[bufferIndex++] = 'u';
                        buffer[bufferIndex++] = t.charAt(tLength - 4);
                        buffer[bufferIndex++] = t.charAt(tLength - 3);
                        buffer[bufferIndex++] = t.charAt(tLength - 2);
                        buffer[bufferIndex++] = t.charAt(tLength - 1);
                    }
                } else {
                    buffer[bufferIndex++] = c;
                }
            }
        }
        sb.append(buffer, 0, bufferIndex);
        sb.append('"');
        return sb.toString();
    }

    private static void fromArray(Object array) {
        StringBuilder builder = builder();
        builder.append("[");
        Class<?> type = array.getClass().getComponentType();
        if (!type.isPrimitive()) {
            Object[] objArr = (Object[]) array;
            for (int i = 0; i < objArr.length; i++) {
                fromObject(objArr[i]);
                if (i != (objArr.length - 1)) {
                    builder.append(",");
                }
            }
        } else if (type == Boolean.TYPE) {
            boolean[] boolArr = (boolean[]) array;
            for (int i = 0; i < boolArr.length; i++) {
                builder.append(boolArr[i]);
                if (i != (boolArr.length - 1)) {
                    builder.append(",");
                }
            }
        } else if (type == Byte.TYPE) {
            byte[] byteArr = (byte[]) array;
            for (int i = 0; i < byteArr.length; i++) {
                builder.append(byteArr[i]);
                if (i != (byteArr.length - 1)) {
                    builder.append(",");
                }
            }
        } else if (type == Short.TYPE) {
            short[] shortArr = (short[]) array;
            for (int i = 0; i < shortArr.length; i++) {
                builder.append(shortArr[i]);
                if (i != (shortArr.length - 1)) {
                    builder.append(",");
                }
            }
        } else if (type == Integer.TYPE) {
            int[] intArr = (int[]) array;
            for (int i = 0; i < intArr.length; i++) {
                builder.append(intArr[i]);
                if (i != (intArr.length - 1)) {
                    builder.append(",");
                }
            }
        } else if (type == Long.TYPE) {
            long[] longArr = (long[]) array;
            for (int i = 0; i < longArr.length; i++) {
                builder.append(longArr[i]);
                if (i != (longArr.length - 1)) {
                    builder.append(",");
                }
            }
        } else if (type == Float.TYPE) {
            float[] floatArr = (float[]) array;
            for (int i = 0; i < floatArr.length; i++) {
                builder.append(floatArr[i]);
                if (i != (floatArr.length - 1)) {
                    builder.append(",");
                }
            }
        } else if (type == Double.TYPE) {
            double[] doubleArr = (double[]) array;
            for (int i = 0; i < doubleArr.length; i++) {
                builder.append(doubleArr[i]);
                if (i != (doubleArr.length - 1)) {
                    builder.append(",");
                }
            }
        } else if (type == Character.TYPE) {
            char[] charArr = (char[]) array;
            for (int i = 0; i < charArr.length; i++) {
                char[] carr = { charArr[i] };
                builder.append(quote(new String(carr)));
                if (i != (charArr.length - 1)) {
                    builder.append(",");
                }
            }
        }
        builder.append("]");
    }

    private static void fromCollection(Collection<Object> coll) {
        StringBuilder builder = builder();
        builder.append("[");
        Iterator<Object> iterator = coll.iterator();
        while (iterator.hasNext()) {
            Object obj = iterator.next();
            fromObject(obj);
            if (iterator.hasNext()) {
                builder.append(",");
            }
        }
        builder.append("]");
    }

    private static void fromMap(Map<Object, Object> map) {
        StringBuilder builder = builder();
        builder.append("{");
        Iterator<Object> iterator = map.keySet().iterator();
        while (iterator.hasNext()) {
            Object key = iterator.next();
            builder.append("\"").append(key).append("\":");
            fromObject(map.get(key));
            if (iterator.hasNext()) {
                builder.append(",");
            }
        }
        builder.append("}");
    }

    private static void fromBean(Object bean) {
        StringBuilder builder = builder();
        builder.append("{");

        try {
            // BeanInfo beanInfo = getBeanInfo(bean.getClass(), bean
            // .getClass().getSuperclass());
            BeanInfo beanInfo = getBeanInfo(bean.getClass(), Object.class);

            PropertyDescriptor[] props = beanInfo.getPropertyDescriptors();
            for (int i = 0; i < props.length; i++) {
                PropertyDescriptor pdesc = props[i];
                String pname = pdesc.getName();
                builder.append("\"").append(pname).append("\":");
                Object[] args = {};
                Object pvalue = pdesc.getReadMethod().invoke(bean, args);
                fromObject(pvalue);
                if (i != (props.length - 1)) {
                    builder.append(",");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        builder.append("}");
    }

    private static BeanInfo getBeanInfo(Class<? extends Object> beanClass, Class<Object> stopClass) throws IntrospectionException {
        BeanInfo beanInfo = beanInfoMap.get(beanClass);
        if (beanInfo == null) {
            beanInfo = Introspector.getBeanInfo(beanClass, stopClass);
            beanInfoMap.put(beanClass, beanInfo);
        }

        return beanInfo;
    }
    
    
    /**
     * json转换为Map
     * @param jsonStr
     * @return
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public static Map jsonToMap(String jsonStr){
		Map jsonMap = new HashMap();
		JSONObject jsonObject = JSONObject.fromObject(jsonStr);
		Set<String> jsonSet = jsonObject.keySet();
		for (String keyStr : jsonSet) {
			jsonMap.put(keyStr, jsonObject.get(keyStr));
		}
		return jsonMap;
	}
	
	/**
	 * json转换为List
	 * @param jsonStr
	 * @return
	 */
	@SuppressWarnings({ "rawtypes" })
	public static List<Map> jsonToList(String jsonStr){
		List<Map> list = new ArrayList<Map>();
		List<String> dataJsonStrs = new ArrayList<String>();
		int findNum = 0;
		while(true){
			findNum = jsonStr.indexOf("{", findNum);
			if(findNum == -1){
				break;
			}
			dataJsonStrs.add(jsonStr.substring(findNum, jsonStr.indexOf("}",findNum)+1));
			findNum++;
		}
		for (String dataJsonStr : dataJsonStrs) {
			Map map = jsonToMap(dataJsonStr);
			list.add(map);
		}
		return list;
	}
	/** 
	* @Title: toJSON 
	* @Description: 对象转json字符串
	* @param obj
	* 			要转换的对象
	* @return
	*        String 
	*/ 
	public static String toJSON(Object obj)
	{
		StringWriter writer = new StringWriter();
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			mapper.writeValue(writer, obj);
		}
		catch (JsonParseException e)
		{
			throw new RuntimeException(e);
		}
		catch (JsonMappingException e)
		{
			throw new RuntimeException(e);
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
		return writer.toString();
	}

	/** 
	* @Title: fromJSON 
	* @Description: json字符串转为对象 
	* @param json
	* 			要转换的json字符串
	* @param clazz
	* 			转换的目标类
	* @return
	*        T 目标类
	*/ 
	public static <T> T fromJSON(String json, Class<T> clazz)
	{
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			return mapper.readValue(json, clazz);
		}
		catch (JsonParseException e)
		{
			throw new RuntimeException(e);
		}
		catch (JsonMappingException e)
		{
			throw new RuntimeException(e);
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	/** 
	* @Title: fromJSON 
	* @Description: json字符串转为对象 
	* @param json
	* 			要转换的json字符串
	* @param t
	* 			转换的结构对象
	* @return
	*        T 目标类
	*/ 
	public static <T>T fromJSON(String json, TypeReference<T> t){
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(json, t);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
