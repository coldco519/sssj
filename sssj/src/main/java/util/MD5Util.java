package main.java.util;


import java.io.UnsupportedEncodingException;
import java.security.*;
import java.util.Arrays;

import org.apache.shiro.codec.Base64;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.util.ByteSource;


/**   
* @Title: MD5Util.java 
* @Description: MD5工具类
* @version V1.0   
*/ 
public class MD5Util
{
	//十六位数字字符串
	private static final String HEX_NUMS_STR = "0123456789ABCDEF";
	//md5字符串长度
	private static final Integer SALT_LENGTH = 12;

	/** 
	* @Title: hexStringToByte 
	* @Description: 将16进制字符串转换成字节数组
	* @version V1.0 
	* @param hex
	* 			16进制字符串
	* @return
	*        byte[] 字节数组
	*/ 
	public static byte[] hexStringToByte(String hex)
	{
		int len = (hex.length() / 2);
		byte[] result = new byte[len];
		char[] hexChars = hex.toCharArray();
		for (int i = 0; i < len; i++)
		{
			int pos = i * 2;
			result[i] = (byte) (HEX_NUMS_STR.indexOf(hexChars[pos]) << 4 | HEX_NUMS_STR.indexOf(hexChars[pos + 1]));
		}
		return result;
	}

	/** 
	* @Title: byteToHexString 
	* @Description: 将指定byte数组转换成16进制字符串
	* @version V1.0 
	* @param b
	* 			指定byte数组
	* @return
	*        String 16进制字符串
	*/ 
	public static String byteToHexString(byte[] b)
	{
		StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < b.length; i++)
		{
			String hex = Integer.toHexString(b[i] & 0xFF);
			if (hex.length() == 1)
			{
				hex = '0' + hex;
			}
			hexString.append(hex.toUpperCase());
		}
		return hexString.toString();
	}

	/** 
	* @Title: validPassword 
	* @Description:  验证口令是否合法
	* @version V1.0 
	* @param password
	* 			算法口令的数据
	* @param passwordInDb
	* 			16进制字符串格式口令
	* @return
	* @throws NoSuchAlgorithmException
	* @throws UnsupportedEncodingException
	*        boolean 
	*/ 
	public static boolean validPassword(String password, String passwordInDb) throws NoSuchAlgorithmException, UnsupportedEncodingException
	{
		// 将16进制字符串格式口令转换成字节数组
		byte[] pwdInDb = hexStringToByte(passwordInDb);
		// 声明盐变量
		byte[] salt = new byte[SALT_LENGTH];
		// 将盐从数据库中保存的口令字节数组中提取出来
		System.arraycopy(pwdInDb, 0, salt, 0, SALT_LENGTH);
		// 创建消息摘要对象
		MessageDigest md = MessageDigest.getInstance("MD5");
		// 将盐数据传入消息摘要对象
		md.update(salt);
		// 将口令的数据传给消息摘要对象
		md.update(password.getBytes("UTF-8"));
		// 生成输入口令的消息摘要
		byte[] digest = md.digest();
		// 声明一个保存数据库中口令消息摘要的变量
		byte[] digestInDb = new byte[pwdInDb.length - SALT_LENGTH];
		// 取得数据库中口令的消息摘要
		System.arraycopy(pwdInDb, SALT_LENGTH, digestInDb, 0, digestInDb.length);
		// 比较根据输入口令生成的消息摘要和数据库中消息摘要是否相同
		if (Arrays.equals(digest, digestInDb))
		{
			// 口令正确返回口令匹配消息
			return true;
		}
		else
		{
			// 口令不正确返回口令不匹配消息
			return false;
		}
	}

	/** 
	* @Title: getEncryptedPwd 
	* @Description: 获得加密后的16进制形式口令
	* @version V1.0 
	* @param password
	* 			算法口令数据
	* @return
	* @throws NoSuchAlgorithmException
	* @throws UnsupportedEncodingException
	*        String 
	*/ 
	public static String getEncryptedPwd(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException
	{
		// 声明加密后的口令数组变量
		byte[] pwd = null;
		// 随机数生成器
		SecureRandom random = new SecureRandom();
		// 声明盐数组变量
		byte[] salt = new byte[SALT_LENGTH];
		// 将随机数放入盐变量中
		random.nextBytes(salt);

		// 声明消息摘要对象
		MessageDigest md = null;
		// 创建消息摘要
		md = MessageDigest.getInstance("MD5");
		// 将盐数据传入消息摘要对象
		md.update(salt);
		// 将口令的数据传给消息摘要对象
		md.update(password.getBytes("UTF-8"));
		// 获得消息摘要的字节数组
		byte[] digest = md.digest();

		// 因为要在口令的字节数组中存放盐，所以加上盐的字节长度
		pwd = new byte[digest.length + SALT_LENGTH];
		// 将盐的字节拷贝到生成的加密口令字节数组的前12个字节，以便在验证口令时取出盐
		System.arraycopy(salt, 0, pwd, 0, SALT_LENGTH);
		// 将消息摘要拷贝到加密口令字节数组从第13个字节开始的字节
		System.arraycopy(digest, 0, pwd, SALT_LENGTH, digest.length);
		// 将字节数组格式加密后的口令转化为16进制字符串格式的口令

		return byteToHexString(pwd);
	}
	
	/**
	 * 加密
	 * 2014年12月3日 上午11:31:04
	 * @param password
	 * @param salt
	 * @return
	 */
	public static String getEncryptedPwdByMD5(String password,String salt){
		return new Md5Hash(password, salt, 1).toString();
	}
	
	/**
	 * 获取盐值
	 * 2014年12月3日 上午11:30:49
	 * @return
	 */
	public static String getSalt(){
		// 随机数生成器
		SecureRandomNumberGenerator random = new SecureRandomNumberGenerator();
		byte[] s = new byte[random.getDefaultNextBytesSize()];
		ByteSource byteSource = random.nextBytes();
		s = byteSource.getBytes();
		return Arrays.toString(s);
	}
	
	public static String encode64ToString(String str){
		String base64Encoded = Base64.encodeToString(str.getBytes());
		return base64Encoded;
	}
	
	public static String decode64ToString(String str){
		String base64Encoded = Base64.decodeToString(str.getBytes()); 
		return base64Encoded;
	}
	
}
