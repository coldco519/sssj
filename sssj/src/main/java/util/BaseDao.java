package main.java.util;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.Version;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Component;

/**
 * 
 * @description Dao 帮助类
 * @version 1.1
 */
@Component("baseDao")
public class BaseDao {
	protected BaseDao(){}
	
	@PersistenceContext(unitName = "jpa")
	private EntityManager entityManager;

	/**
	 * 根据主键查询
	 * @param clazz <strong>目标实体Class对象</strong>
	 * @param id   <strong>主键</strong>
	 * @return
	 */
	public <T>T findEntity(Class<T> clazz, Serializable id){
		return entityManager.find(clazz, id);
	}
	
	/**
	 * 持久化实体
	 * @param t
	 */
	public <T>void save(T t){
		entityManager.persist(t);
	}
	
	
	/**
	 * 查询单实体结果集
	 * @param clazz   <strong>目标实体Class对象</strong>
	 * @param conditionObj   参数对象 
	 * @return
	 */
	public <T>List<T> getResultList(Class<T> clazz,T conditionObj){
		return getResultList(clazz, conditionObj, null);
	}
	/**
	 * 该方法之供单实体查询 如果需求内部含有连接查询 或复杂查询  ，请使用 @see {@link #getResultList(Integer, Integer, String, Object...)}
	 * 
	 * @param clazz  目标实体Class类
	 * @param conditionObj  参数对象 
	 * @param conditionMap  key : 字段名 ，value ： >,<= ，!= 等单项比较
	 * @param wherePart   where后的字段拼接  
	 * @param orderByFields  实体的排序字段
	 * @param isDESC
	 * @param start
	 * @param pageSize
	 * @return
	 */
	public <T>List<T> getResultList(Class<T> clazz,T conditionObj,Map<String, String> conditionMap,
			String wherePart, String[] orderByFields,Boolean isDESC,Integer start,Integer pageSize) {
		String jpql = "SELECT u FROM " + clazz.getName() + " u WHERE 1=1 ";
		Field[] fields = null;
		Map<String, Object> map = null;
		if (conditionObj != null) {
			map = new HashMap<String, Object>();
			fields = clazz.getDeclaredFields();
			for (int i = 0; i < fields.length; i++) {
				Field field = fields[i];
				if (Modifier.isStatic(field.getModifiers()) || field.isAnnotationPresent(Version.class)) {
					continue;
				}
				try {
					if (isPrimitiveDefaultValue(field, conditionObj)) {
						continue;
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				String fieldName = field.getName();
				field.setAccessible(true);
				try {
					Object value = field.get(conditionObj);
					if (value != null) {
						if (Arrays.asList(value.getClass().getInterfaces()).contains(Collection.class) || Arrays.asList(value.getClass().getInterfaces()).contains(Map.class)) {
							continue;
						}
//						placeholder
						String placeholder = "=";
						String newHolder = null;
						if (conditionMap != null && !conditionMap.isEmpty() && (newHolder = conditionMap.get(fieldName)) != null) {
							placeholder = newHolder;
						}
						map.put(fieldName, value);
						jpql += " and u." + fieldName + " " +  placeholder +" :" + fieldName + " ";
					}
				} catch (Exception e) {
					e.printStackTrace();
				} 
			}
		}
		if (wherePart != null) {
			jpql += " and " + wherePart;
		}
		String tail = "";
		
		boolean isOrderBy = false;
		if (orderByFields != null && orderByFields.length > 0) {
			for (int i = 0; i < orderByFields.length; i++) {
				String orderByName = orderByFields[i];
				try {
					if(clazz.getDeclaredField(orderByName) != null){
						tail += "u." + orderByName + ",";
					}
				} catch (Exception e) {
					e.printStackTrace();
				} 
			}
			if (tail.length()>0) {
				tail = tail.substring(0, tail.length()-1);
			}
		}
		
		if (tail.length() > 0) {
			jpql = jpql + " ORDER BY " + tail;
			isOrderBy = true;
		}
		if (isOrderBy) {
			if (isDESC) {
				jpql += " DESC";
			}
			
			if (!isDESC) {
				jpql +=" ASC";
			}
		}
		TypedQuery<T> query = entityManager.createQuery(jpql, clazz);
		if (map != null && !map.isEmpty()) {
			Set<Map.Entry<String, Object>> entries= map.entrySet();
			for(Iterator<Map.Entry<String, Object>> iterator = entries.iterator();iterator.hasNext();){
				Map.Entry<String, Object> entry = iterator.next();
				query.setParameter(entry.getKey(),entry.getValue());
			}
		}
		if (pageSize != null && pageSize >0) {
			query.setFirstResult((start != null && start >-1 ) ? start : 0);
			query.setMaxResults(pageSize);
		}
		return query.getResultList();
	}
	
	/**
	 * 
	 * @param clazz   <strong>目标实体Class对象</strong>
	 * @param conditionObj   参数对象 
	 * @param wherePart  where后的条件
	 * @return
	 */
	public <T>List<T> getResultList(Class<T> clazz,T conditionObj,String wherePart){
		return getResultList(clazz, conditionObj, null, wherePart, null, null, null, null);
	}
	public <T>List<T> getResultList(Class<T> clazz,T conditionObj,int start,int pageSize){
		return getResultList(clazz, conditionObj, null, null, null, null, start, pageSize);
	}
	
	/**
	 *  位置法查询 可用于复杂的链接查询等等。。
	 * @param start
	 * @param pageSize
	 * @param jpql
	 * @param conditionSorted
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T>List<T> getResultList(Integer start,Integer pageSize,String jpql,Object... conditionSorted){
		Query query = entityManager.createQuery(jpql);
		if (conditionSorted != null && conditionSorted.length > 0) {
			for (int i = 1; i < conditionSorted.length + 1; i++) {
				query.setParameter(i, conditionSorted[i -1]);
			}
		}
		if (start != null && start > -1 && pageSize != null && pageSize >0) {
			query.setFirstResult(start);
			query.setMaxResults(pageSize);
		}
		
		return query.getResultList();
	}
	/**
	 *  ---位置法查询 可用于复杂的链接查询等等。。
	 *  ---仅限于原生sql,但是返回clazz实体的list对象
	 * @param start
	 * @param pageSize
	 * @param jpql
	 * @param conditionSorted
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T>List<T> getResultListByNativeSQL(Integer start,Integer pageSize,String jpql,Class<T> clazz,Object... conditionSorted){
		Query query = entityManager.createNativeQuery(jpql);
		query.unwrap(SQLQuery.class).setResultTransformer(Transformers.aliasToBean(clazz));
		if (conditionSorted != null && conditionSorted.length > 0) {
			for (int i = 1; i < conditionSorted.length + 1; i++) {
				query.setParameter(i, conditionSorted[i -1]);
			}
		}
		if (start != null && start > -1 && pageSize != null && pageSize >0) {
			query.setFirstResult(start);
			query.setMaxResults(pageSize);
		}
		
		return query.getResultList();
	}
	/**
	 * 
	 * @description 分页查询
	 * @param start
	 * @param pageSize
	 * @param jpql
	 * @param map
	 * @return
	 */
	public <T>Page<T> getResultListMap(Integer start,Integer pageSize,String jpql,Map<String,Object> map){
		Query q = entityManager.createQuery(jpql);
		if (map != null && !map.isEmpty()) {
			Set<Map.Entry<String, Object>> enties = map.entrySet();
			for (Iterator<Map.Entry<String, Object>> iterator = enties.iterator(); iterator.hasNext();) {
				Map.Entry<String, Object> entry =  iterator.next();
				q.setParameter(entry.getKey(), entry.getValue());
			}
		}
		if (start != null && start > -1 && pageSize != null && pageSize >0) {
			q.setFirstResult(start);
			q.setMaxResults(pageSize);
		}
		@SuppressWarnings("unchecked")
		List<T> result = q.getResultList();
		long count = getCount(jpql, map);
		return new Page<T>(start == null ? 0 :start, count, pageSize == null ? Integer.MAX_VALUE : pageSize, result);
	}
	
	@SuppressWarnings("unchecked")
	public <T>List<T> getResultListMap(String jpql,Map<String,Object> map){
		Query q = entityManager.createQuery(jpql);
		if (map != null && !map.isEmpty()) {
			Set<Map.Entry<String, Object>> enties = map.entrySet();
			for (Iterator<Map.Entry<String, Object>> iterator = enties.iterator(); iterator.hasNext();) {
				Map.Entry<String, Object> entry =  iterator.next();
				q.setParameter(entry.getKey(), entry.getValue());
			}
		}
		return q.getResultList();
	}
	
	public long getCount(String jpql,Map<String,Object> map){
		return getResultListMap(jpql, map).size();
	}
	
	@SuppressWarnings("unchecked")
	public <T>T getSingleResult(String jpql,Object... conditionSorted){
		Query query = entityManager.createQuery(jpql);
		if (conditionSorted != null && conditionSorted.length > 0) {
			for (int i = 1; i < conditionSorted.length + 1; i++) {
				query.setParameter(i, conditionSorted[i -1]);
			}
		}
		return (T) query.getSingleResult();
	}
	
	/**
	 * 分页查询 
	 * @param start  起始位置
	 * @param pageSize  步长
	 * @param jpqlQuery  查询语句
	 * @param jpqlCount  count查询语句
	 * @param conditionSorted  位置参数数组
	 * @return  封装后的数据对象
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Page getResultListWithPage4Private(Integer start,Integer pageSize,String jpqlQuery,String jpqlCount,Object... conditionSorted){
		List result = getResultList(start, pageSize, jpqlQuery, conditionSorted);
		Query query = entityManager.createQuery(jpqlCount);
		if (conditionSorted != null && conditionSorted.length > 0) {
			for (int i = 1; i < conditionSorted.length + 1; i++) {
				query.setParameter(i, conditionSorted[i - 1]);
			}
		}
		long count = query.getResultList().size();
		start = start == null ? 0 : start;
		pageSize = pageSize == null ? Integer.MAX_VALUE : pageSize;
		return new Page(start, count, pageSize, result);
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Page getResultListWithPage(Integer start,Integer pageSize,String jpqlQuery,String jpqlCount,Object... conditionSorted){
		List result = getResultList(start, pageSize, jpqlQuery, conditionSorted);
		Query query = entityManager.createQuery(jpqlCount);
		if (conditionSorted != null && conditionSorted.length > 0) {
			for (int i = 1; i < conditionSorted.length + 1; i++) {
				query.setParameter(i, conditionSorted[i - 1]);
			}
		}
		long count = 0;
		if(query.getResultList().size()==0){
			
		}else{
			count = (Long) query.getSingleResult();
		}
		start = start == null ? 0 : start;
		pageSize = pageSize == null ? Integer.MAX_VALUE : pageSize;
		return new Page(start, count, pageSize, result);
	}
	
	public long getCount(String jpql){
		return (Long)(entityManager.createQuery(jpql).getSingleResult());
	}
	
	public long getCount(String jpql,Object... conditionSorted){
		Query query = entityManager.createQuery(jpql);
		if (conditionSorted != null && conditionSorted.length > 0) {
			for (int i = 1; i < conditionSorted.length + 1; i++) {
				query.setParameter(i, conditionSorted[i - 1]);
			}
		}
		return (Long)(entityManager.createQuery(jpql).getSingleResult());
	}
	
	
	public <T>List<T> getResultList(String jpql,Object... conditionSorted){
		return getResultList(null, null, jpql, conditionSorted);
	}
	/**
	 * add by sunpf 2014-4-4
	 * @param jpql
	 * @param clazz
	 * @param conditionSorted
	 * @return
	 */
	public <T>List<T> getResultListByNativeSql(String jpql,Class<T> clazz,Object... conditionSorted){
		return getResultListByNativeSQL(null, null, jpql,clazz, conditionSorted);
	}
	@SuppressWarnings("unchecked")
	public <T>List<T> getResultListByNamedQuery(String namedQueryName,Object... conditionSorted){
		Query query = entityManager.createNamedQuery(namedQueryName);
		if (conditionSorted != null) {
			for (int i = 1; i < conditionSorted.length + 1; i++) {
				query.setParameter(i, conditionSorted[i - 1]);
			}
		}
		return query.getResultList();
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Page getResultListByNamedQueryWithPage(Integer start,Integer pageSize, String namedQueryName,String QueryCount,Object... conditionSorted){
		List<?> result = getResultListByNamedQuery(namedQueryName, conditionSorted);
		Query query = entityManager.createNamedQuery(namedQueryName);
		if (conditionSorted != null) {
			for (int i = 1; i < conditionSorted.length + 1; i++) {
				query.setParameter(i, conditionSorted[i - 1]);
			}
		}
		if (start != null && start >=0 && pageSize != null && pageSize >0) {
			query.setFirstResult(start);
			query.setMaxResults(pageSize);
		}
		long count = getCount(QueryCount,conditionSorted);
		return new Page(start==null ? 0 : start, count, pageSize==null ? Integer.MAX_VALUE : pageSize, result);
	}
	private <T>Boolean isPrimitiveDefaultValue(Field field,T obj) throws Exception{
		field.setAccessible(true);
		Class<?> type = field.getType();
		if(!type.isPrimitive()){
			return false;
		}
		@SuppressWarnings("unused")
		int val = -1;
		if (type == int.class && (val =field.getInt(obj)) == 0) {
			return true;
		}
		if (type == long.class && field.getLong(obj) == 0l) {
			return true;
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getResultListBySql(String sql){
		return entityManager.createNativeQuery(sql).getResultList();
	}
	@SuppressWarnings("unchecked")
	public <T>List<T> getResultListBySql(String sql,Class<T> clazz){
		return entityManager.createNativeQuery(sql,clazz).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public <T>List<T> getResultListBySql(String sql,Class<T> clazz,Object... params){
		Query q = entityManager.createNativeQuery(sql, clazz);
		for (int i = 1; i <= params.length; i++) {
			q.setParameter(i, params[i-1]);
		}
		return q.getResultList();
	}
	
	/**
	 * 刷新实体 要求entityManager-managed
	 * @param entity
	 */
	public  <T>void refreshEntity(T entity){
		entityManager.refresh(entity);
	}
	
	/**
	 * 删除实体
	 * @param entity
	 */
	public  <T>void removeEntity(T entity){
		entityManager.remove(entity);
	}
	
	/**
	 * @param entity
	 * @return copy of entity
	 */
	public <T>T saveOrUpdateEntity(T entity){
		return entityManager.merge(entity);
	}
	
	/**
	 *  根据jpql进行更新操作 
	 * @param jpql
	 * @return
	 */
	public int updateByJpql(String jpql){
		return updateByJpql(jpql, new Object[]{});
	}
	
	public int updateByJpql (String jpql,Object... conditionSorted){
		Query query = entityManager.createQuery(jpql);
		if (conditionSorted != null && conditionSorted.length > 0) {
			for (int i = 1; i < conditionSorted.length + 1; i++) {
				query.setParameter(i, conditionSorted[i - 1]);
			}
		}
		return query.executeUpdate();
	}
	
	/**
	 * 执行sql更新
	 * @param sql
	 * @return
	 */
	public int executeUpdate(String sql){
		return entityManager.createNativeQuery(sql).executeUpdate();
	}
	
	public void clear()
	{
		entityManager.clear();
	}
	
	/**
	 * 锁行的查询
	 */
	@SuppressWarnings("unchecked")
	public <T>List<T> getResultListByJpqlLockRow(String jpql){
		Query query = entityManager.createQuery(jpql);
		query.setLockMode(LockModeType.PESSIMISTIC_WRITE);
		return query.getResultList();
	}
	
	/**
	 * 锁行的查询 
	 * -- 执行原生sql时锁行
	 */
	@SuppressWarnings("unchecked")
	public <T>List<T> getResultListByNativeSqlLockRow(String sql){
		Query query = entityManager.createNativeQuery(sql);
		query.setLockMode(LockModeType.PESSIMISTIC_WRITE);
		return query.getResultList();
	}
//	/**
//	 * 锁行的更新
//	 */
//	public int updateByJpqlLockRow(String jpql){
//		Query query = entityManager.createQuery(jpql);
//		query.setLockMode(LockModeType.PESSIMISTIC_WRITE);
//		return query.executeUpdate();
//	}
	
}
