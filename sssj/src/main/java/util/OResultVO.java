package main.java.util;

import java.io.Serializable;

public class OResultVO implements Serializable{

	private static final long serialVersionUID = 1L;

	private boolean flag;
	
	private String message;

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public OResultVO(boolean flag, String message) {
		super();
		this.flag = flag;
		this.message = message;
	}

	public OResultVO() {
		super();
	}

	public OResultVO(boolean flag) {
		super();
		this.flag = flag;
	}
	
}
