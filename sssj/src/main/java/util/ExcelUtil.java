package main.java.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


/**
 * 
 * @Title: ExcelUtil.java
 * @Description:execl上传导入工具类【目前只支持2003版本】 
 * @version V1.0 
 *
 */
public class ExcelUtil {
	/**

     * 设置Excel头部信息

     * @return

     */
    public static final String setExcelHeader(){

       StringBuilder sb = new StringBuilder();

       sb.append("<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">");

        sb.append(" <head>");

        sb.append(" <!--[if gte mso 9]><xml>");

        sb.append("<x:ExcelWorkbook>");

        sb.append("<x:ExcelWorksheets>");

        sb.append("<x:ExcelWorksheet>");

        sb.append("<x:Name></x:Name>"); 

        sb.append("<x:WorksheetOptions>");

        sb.append("<x:Print>");            

        sb.append("<x:ValidPrinterInfo />");

        sb.append(" </x:Print>"); 

        sb.append("</x:WorksheetOptions>");

        sb.append("</x:ExcelWorksheet>");

        sb.append("</x:ExcelWorksheets>");

        sb.append("</x:ExcelWorkbook>");

        sb.append("</xml>");

        sb.append("<![endif]-->"); 

        sb.append(" </head>"); 

        sb.append("<body>");

        return sb.toString();

    }
    /**

     * 设置Excel尾部信息

     * @return

     */

    public static final String setExcelFoot(){

       StringBuilder sb = new StringBuilder();

       sb.append("</body>");

       sb.append("</html>");

       return sb.toString();

    }

    /**

     * 设置Excel内容中的标题(第一行)

     * @return 返回多个单元格的数据(包括<td>标签和</td>标签)

     */

    public static String setTitle(String[] titles){

       StringBuilder sb = new StringBuilder();

       for (int i = 0; i < titles.length; i++) {
    	   sb.append("<td align='center' valign='middle' >");
           String title = titles[i];

           sb.append(title);

           sb.append("</td>");

       }

       return sb.toString();

    }
    
    /** 
    * @Title:SaveFileFromInputStream
    * @Description: 将文件流保存到文件中
    * @param stream
    * @param path
    * @param savefile
    * @throws IOException
    */ 
    public static void SaveFileFromInputStream(InputStream stream,String path,String savefile) throws IOException
    {      
        FileOutputStream fs=new FileOutputStream( path + "/"+ savefile);
        byte[] buffer =new byte[1024*1024];
        int byteread = 0; 
        while ((byteread=stream.read(buffer))!=-1)
        {
           fs.write(buffer,0,byteread);
           fs.flush();
        } 
        fs.close();
        stream.close();      
    }
    
    /** 
    * @Title:readXLSExcelFileInfo
    * @Description: 读取xls格式的文件。
    * @param xlsFilePath  文件路径
    * @param sheetIndex   工作表在文件中的索引值
    * @return 
    * 			List<Map<String,String>>
    * 			每一行数据封装成一个Map,以文件第一行为key。所有行数据组成一个List
    * <p>
    * 	例如：
    * 			**********************
    * 			*  id  * name * age  *
    * 			*------*------*------*
    * 			*  1   * lily * 20   *
    * 			*------*------*------*
    * 			*  2   * tom  * 30   *
    * 			**********************
    * List<Map<String,String>> list = read this file
    * list.get(0).get("id")  = 1
    * list.get(1).get("name") = tom
    * </p>
    * @throws BiffException
    * @throws IOException
    */ 
    public static List<Map<String,String>> readXLSExcelFileInfo(String xlsFilePath,int sheetIndex) throws BiffException, IOException {
    	/*******
    	 * 实现方法一：
    	 * 
    	 * 使用POI的HSSFWorkbook,HSSFSheet,HSSFRow,HSSFCell来解析xls文件
    	 * 效率高，测试文件解析时长是方法二的2/3
    	 * 但是解析出来的value格式不是很正确，需要逐个进行对比纠正。
    	 * 
    	 * 
    	//获得book
    	HSSFWorkbook hssfWorkbook = new HSSFWorkbook(new FileInputStream(xlsFilePath));
    	if(hssfWorkbook == null || hssfWorkbook.getNumberOfSheets() <= 0){
    		return null;
    	}
    	//获得需要读取的sheet
    	HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(sheetIndex);
    	int rownum = hssfSheet.getLastRowNum();
    	if(rownum <= 0){
    		return null;
    	}
    	//返回的结果集
    	List<Map<String,String>> entityList = new ArrayList<Map<String,String>>();
    	//用来存储sheet中第一行的列表
		List<String> keyList = new ArrayList<String>();
		
    	for (int i = 0; i < rownum; i++) {                    //循环每一行
    		HSSFRow row = hssfSheet.getRow(i);                //获得一行数据
    		Map<String,String> entity 
    				= new LinkedHashMap<String, String>();   //用来存储这一行数据  
    		for(int j = 0; j < row.getPhysicalNumberOfCells(); j++){   //循环这一样的每一列数据
    			HSSFCell cell = row.getCell(j);               //获得这个单元格
				if(cell == null){
					continue;
				}
				String value = cell.toString();
				if(i == 0){                                   //如果是第一行的数据
					keyList.add(value);                       //就把内容存入key的列表中
					continue;
				}
				entity.put(keyList.get(j), value);            //不是第一行，则把内容存入map
			}
			if(i==0) continue;                               //如果是第一行，则不把map存入list
			entityList.add(entity);                          //不是第一行，则把map存入list
    	}
    	return entityList;
    	********/
    	/************
    	 * 实现方法二：
    	 * 使用jxl的Workbook来解析文件
    	 * 效率相对于方法一，效率低，时长是方法一的1.5倍，但是解析出来的内容与实际内容真实相同。
    	 * 
    	 * ******/
    	//获得workbook
		Workbook wb = Workbook.getWorkbook(new File(xlsFilePath));
		//获得所有sheets
		Sheet[] sheets = wb.getSheets();
		if(sheets == null || sheets.length <= 0){
			return null;
		}
		
		Sheet sheet = sheets[sheetIndex];                      //获得需要解析的sheet
		int rowNum = sheet.getRows();                          //获得行数
		int columnNum = sheet.getColumns();                    //获得列数
		List<Map<String,String>> entityList = 
				new ArrayList<Map<String,String>>();           //返回的结果集
		List<String> keyList = new ArrayList<String>();        //存储key的列表
		for (int i = 0; i < rowNum; i++) {                     //循环每一行
			Map<String,String> entity = 
					new LinkedHashMap<String, String>();       //用来存储这一行数据
			for (int j = 0; j < columnNum; j++) {              //循环这一行的所有列
				Cell cell = sheet.getCell(j,i);                //获得单元格
				if(StringUtil.isEmpty(cell.getContents())){
					continue;
				}
				String value = 
						StringUtil.reTrimByString(cell.getContents());
				if(i == 0){                                    //如果是第一行
					keyList.add(value);                        //将内容存入key的列表
					continue;
				}
				entity.put(keyList.get(j), value);             //不是第一行，则把内容存入map
			}
			if(i==0) continue;                                 //如果是第一行，不把内容存入结果集
			entityList.add(entity);                            //不是第一行，则把内容存入结果集
		}
		return entityList;
		
		
	}
    
    
    
    /** 
    * @Title:readXLSXExcelFileInfo
    * @Description: 读取xlsx格式的文件,使用POI的XSSFWorkbook等类方法。
    * @param xlsxFilePath   文件路径
    * @param sheetIndex     工作表在文件中的索引值
    * @return 
    * 				List<Map<String,String>>   参考方法：@readXLSExcelFileInfo
    * @throws IOException
    */ 
    public static List<Map<String,String>> readXLSXExcelFileInfo(String xlsxFilePath,int sheetIndex) throws IOException{
        // 构造 XSSFWorkbook 对象，xlsFilePath 传入文件路径
        XSSFWorkbook xwb = new XSSFWorkbook(new FileInputStream(xlsxFilePath));
        if(xwb.getNumberOfSheets() <= 0){
        	return null;
        }
        // 读取第一章表格内容
        XSSFSheet sheet = xwb.getSheetAt(sheetIndex);
        // 定义 row、cell
        XSSFRow row;
        String cell;
        List<Map<String,String>> entityList = 
        		new ArrayList<Map<String,String>>();            //返回的结果集
        
		List<String> keyList = new ArrayList<String>();         //用来存储第一行key的列表
        // 循环输出表格中的内容
        for (int i = sheet.getFirstRowNum(); i < sheet.getPhysicalNumberOfRows(); i++) {
            row = sheet.getRow(i);
            Map<String,String> entity = new LinkedHashMap<String, String>();
            int colunms = sheet.getRow(0).getPhysicalNumberOfCells();
            for (int j = row.getFirstCellNum(); j < colunms; j++) {
                // 获取单元格内容，
                cell = StringUtil.isEmpty(row.getCell(j))?"":row.getCell(j).toString();
				if(StringUtil.isEmpty(cell)){
					continue;
				}
				String value = StringUtil.reTrimByString(cell);
				if(i == 0){                                  //如果是第一行
					keyList.add(value);                      //将内容存入key的列表
					continue;
				}
				entity.put(keyList.get(j), value);           //不是第一行，则把内容存入map
			}
			if(i==0) continue;                               //如果是第一行，不把内容存入结果集
			entityList.add(entity);                          //不是第一行，则把内容存入结果集
        }
		return entityList;
    }
    
    /**
     * 
     * @Title: readXLSXSheetNameList
     * @Description:读取2007+获取sheet列表 
     * @param xlsxFilePath
     * @return
     *		List<String>
     */
    public static List<String> readXLSXSheetNameList(String xlsxFilePath){
    	List<String> list = new ArrayList<String>();
    	try {
			XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(xlsxFilePath));
			
			Iterator<XSSFSheet> iterator = wb.iterator();  
            while (iterator.hasNext()) {  
                list.add(iterator.next().getSheetName());  
            }  
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	return list;
    }
    
    public static List<String> readXLSSheetNameList(String xlsFilePath){
    	List<String> list = new ArrayList<String>();
    	try {
			HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(xlsFilePath));
			
			int i = 0;  
            while (true) {  
                try {  
                    String name = wb.getSheetName(i);  
                    list.add(name);  
                    i++;  
                } catch (Exception e) {  
                    break;  
                }  
            }  
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}  
    	return list;
    }
}
