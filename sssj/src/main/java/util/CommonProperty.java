package main.java.util;


import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

/**   
* @Title: CommonProperty.java 
* @Description: 读取配置文件(*-*.properties)，将配置读取到内存中.
* @version V1.0   
*/ 
public class CommonProperty extends PropertyPlaceholderConfigurer{
	// 保存属性的map
	private Map<String, String> resolvedProps;

    @Override
    protected void processProperties(ConfigurableListableBeanFactory beanFactoryToProcess, Properties props)
            throws BeansException {
        super.processProperties(beanFactoryToProcess, props);
        resolvedProps = new HashMap<String, String>();
        for (Object key : props.keySet()) {
            String keyStr = key.toString();
            resolvedProps.put(keyStr, props.getProperty(keyStr));
        }
        
        PISConstant.setProperties(resolvedProps);
    }

    public Map<String, String> getResolvedProps() {
        return resolvedProps;
    }

    public void setResolvedProps(Map<String, String> resolvedProps) {
        this.resolvedProps = resolvedProps;
    }
}
