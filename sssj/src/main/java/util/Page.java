package main.java.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Title: Page.java
 * @Description: 分页对象. 包含当前页数据及分页信息如总记录数.
 * @version V1.0
 */
public class Page<T> implements Serializable {
    private static final long serialVersionUID = -3017723240787536531L;

    // 默认每页显示条数
    private static final int DEFAULT_PAGE_SIZE = 15;

    // 每页的记录数
    private int pageSize = DEFAULT_PAGE_SIZE;

    // 当前页第一条数据在List中的位置,从0开始
    private long start;

    // 当前页中存放的记录,类型一般为List
    private List<T> data;

    // 总记录数
    private long totalCount;

    private transient String gift;

    /**
     * 
     * @Description: 构造方法，只构造空页.
     * 
     */
    public Page() {
        this(0, 0, DEFAULT_PAGE_SIZE, new ArrayList<T>());
    }

    /**
     * 
     * @Description: 默认构造方法.
     * @param start
     *            本页数据在数据库中的起始位置
     * @param totalSize
     *            数据库中总记录条数
     * @param pageSize
     *            本页容量
     * @param data
     *            本页包含的数据
     */
    public Page(long start, long totalSize, int pageSize, List<T> data) {
        this.pageSize = pageSize;
        this.start = start;
        this.totalCount = totalSize;
        this.data = data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    /**
     * @Title: getTotalCount
     * @Description: 取总记录数.
     * @return long
     */
    public long getTotalCount() {
        return this.totalCount;
    }

    /**
     * @Title: getTotalPageCount
     * @Description: 取总页数
     * @return long
     */
    public long getTotalPageCount() {
        if (totalCount % pageSize == 0)
            return totalCount / pageSize;
        else
            return totalCount / pageSize + 1;
    }

    /**
     * @Title: getPageSize
     * @Description: 取每页数据容量.
     * @return int
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * @Title: getResult
     * @Description: 取当前页中的记录.
     * @return List
     */
    public List<T> getResult() {
        return data;
    }

    /**
     * @Title: getCurrentPageNo
     * @Description: 取该页当前页码,页码从1开始
     * @return long
     */
    public long getCurrentPageNo() {
        return start / pageSize + 1;
    }

    /**
     * @Title: 该页是否有下一页.
     * @Description: @TODO
     * @return boolean
     */
    public boolean isHasNextPage() {
        return this.getCurrentPageNo() < this.getTotalPageCount();
    }

    /**
     * @Title: isHasPreviousPage
     * @Description:该页是否有上一页.
     * @return boolean
     */
    public boolean isHasPreviousPage() {
        return this.getCurrentPageNo() > 1;
    }

    /**
     * @Title: getStartOfPage
     * @Description: 获取任一页第一条数据在数据集的位置，每页条数使用默认值.
     * @see #getStartOfPage(int,int)
     * @param pageNo
     * @return int
     */
    protected static int getStartOfPage(int pageNo) {
        return getStartOfPage(pageNo, DEFAULT_PAGE_SIZE);
    }

    /**
     * @Title: getStartOfPage
     * @Description: 获取任一页第一条数据在数据集的位置
     * @param pageNo
     *            从1开始的页号
     * @param pageSize
     *            每页记录条数
     * @return int 该页第一条数据
     */
    public static int getStartOfPage(int pageNo, int pageSize) {
        return (pageNo - 1) * pageSize;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public static int getDefaultPageSize() {
        return DEFAULT_PAGE_SIZE;
    }

    public String getGift() {
        return gift;
    }

    public void setGift(String gift) {
        this.gift = gift;
    }

    public boolean hasGift() {
        return this.gift != null;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

}
