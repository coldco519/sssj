package main.java.util;


import java.util.HashMap;
import java.util.Map;

/**
 * @Title: PISConstant.java
 * @Description: 工程上下文类，主要操作配置文件内容的读取
 */
public class PISConstant {

    // 配置文件生成的map
    @SuppressWarnings("rawtypes")
    private static HashMap properties;

    /**
     * 每页数据显示15条
     */
    public static final int PAGE_SIZE = 15;

    @SuppressWarnings("rawtypes")
    public static void setProperties(Map<String, String> maps) {
        if (maps != null && maps.size() > 0) {
            PISConstant.properties = (HashMap) maps;
        }
    }

    public static String getProValue(String key) {
        String returnValue = (String) properties.get(key);
        if (returnValue == null || "".equals(returnValue))
            returnValue = "";
        return returnValue.trim();
    }
}
