package main.java.login.service;

import java.util.List;

import main.java.login.entity.ORolePO;
import main.java.login.entity.OUserPO;

/**
 * 用户角色关系
 * @author cOde mOnkey
 * 2014年12月4日 下午3:47:10
 */
public interface IUserRoleRelationService {

	
	public ORolePO getRolePOByUserId(Long userId);
	
	public List<OUserPO> getUserPOByRoleId(Long roleId);
}
