package main.java.login.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import main.java.general.enums.FLAGENUM;
import main.java.login.dao.IUserRoleRelationDao;
import main.java.login.entity.ORolePO;
import main.java.login.entity.OUserPO;
import main.java.login.entity.OUserRoleRelationPO;
import main.java.login.service.IUserRoleRelationService;
import main.java.util.StringUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserRoleRelationServiceImpl implements IUserRoleRelationService{

	@Autowired
	private IUserRoleRelationDao iUserRoleRelationDao;
	
	@Override
	public ORolePO getRolePOByUserId(Long userId) {
		OUserRoleRelationPO userRoleRelationPO = new OUserRoleRelationPO();
		userRoleRelationPO.setUserPO(new OUserPO(userId));
		userRoleRelationPO.setAvailable(FLAGENUM.TRUE);
		List<OUserRoleRelationPO> list = iUserRoleRelationDao.findUserRoleRelationPO(userRoleRelationPO);
		if(!StringUtil.isEmpty(list)){
			return list.get(0).getRolePO();
		}else{
			return null;
		}
	}

	@Override
	public List<OUserPO> getUserPOByRoleId(Long roleId) {
		OUserRoleRelationPO userRoleRelationPO = new OUserRoleRelationPO();
		userRoleRelationPO.setRolePO(new ORolePO(roleId));
		userRoleRelationPO.setAvailable(FLAGENUM.TRUE);
		List<OUserRoleRelationPO> list = iUserRoleRelationDao.findUserRoleRelationPO(userRoleRelationPO);
		if(!StringUtil.isEmpty(list)){
			List<OUserPO> result = new ArrayList<OUserPO>();
			for(OUserRoleRelationPO po : list){
				result.add(po.getUserPO());
			}
			return result;
		}
		
		return Collections.emptyList();
	}

}
