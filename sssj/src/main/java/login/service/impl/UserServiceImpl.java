package main.java.login.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import main.java.general.enums.FLAGENUM;
import main.java.login.dao.IUserDao;
import main.java.login.entity.OUserPO;
import main.java.login.entity.OUserVO;
import main.java.login.service.IUserService;
import main.java.util.MD5Util;
import main.java.util.OResultVO;
import main.java.util.StringUtil;

@Service
@Transactional
public class UserServiceImpl implements IUserService{

	@Autowired
	private IUserDao iUserDao;
	
	@Override
	public OResultVO toRegistUser(String username, String password) {
		
		OUserPO userPO = new OUserPO();
		userPO.setUsername(username);
		userPO.setLocked(FLAGENUM.FALSE);
		
		List<OUserPO> list = iUserDao.findUserByUser(userPO);
		
		if(!StringUtil.isEmpty(list)){
			return new OResultVO(false);
		}
		
		try {
			String salt = MD5Util.getSalt();
			userPO.setPassword(MD5Util.getEncryptedPwdByMD5(password, salt));
			userPO.setSalt(MD5Util.encode64ToString(salt));
			iUserDao.save(userPO);
		} catch (Exception e) {
		}
		return new OResultVO(true);
	}

	@Override
	public OUserPO login(String username, String password) {
		OUserPO userPO = new OUserPO();
		userPO.setUsername(username);
		List<OUserPO> list = iUserDao.findUserByUser(userPO);
		
		if(StringUtil.isEmpty(list)){
			return null;
		}
		
		OUserPO loginUser = list.get(0);
		String salt = MD5Util.decode64ToString(loginUser.getSalt());
		if(MD5Util.getEncryptedPwdByMD5(password, salt).equals(loginUser.getPassword())){
			return loginUser;
		}
		return null;
	}

	@Override
	public List<OUserPO> getUsers() {
		String jpql = "from OUserPO";
		return iUserDao.getBaseDao().getResultList(jpql);
	}

	@Override
	public List<OUserVO> getUsersVO() {
		List<OUserPO> result = this.getUsers();
		if(!StringUtil.isEmpty(result)){
			List<OUserVO> list = new ArrayList<OUserVO>();
			for(OUserPO po : result){
				list.add(OUserVO.from(po));
			}
			return list;
		}
		return Collections.emptyList();
	}

	@Override
	public OUserPO getUserPObyUsername(String currentUsername) {
		if(!StringUtil.isEmpty(currentUsername)){
			String jpql = "from OUserPO where username = '"+currentUsername+"'";
			List<OUserPO> list = iUserDao.getBaseDao().getResultList(jpql);
			if(!StringUtil.isEmpty(list)){
				return list.get(0);
			}
		}
		return null;
	}

	@Override
	public OUserPO getUserById(Long id) {
		return iUserDao.findUserById(id);
	}

}
