package main.java.login.dao;

import java.util.List;

import main.java.login.entity.OUserRoleRelationPO;
import main.java.util.BaseDao;

public interface IUserRoleRelationDao {
	
	public BaseDao getBaseDao();

	public void save(OUserRoleRelationPO userRoleRelationPO);
	
	public  List<OUserRoleRelationPO> findUserRoleRelationPO(OUserRoleRelationPO userRoleRelationPO);
	
	public OUserRoleRelationPO update(OUserRoleRelationPO userRoleRelationPO);
	
	public List<OUserRoleRelationPO> findRelationPOsByJpql(String jpql);
}
