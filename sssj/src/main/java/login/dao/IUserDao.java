package main.java.login.dao;

import java.util.List;

import main.java.login.entity.OUserPO;
import main.java.util.BaseDao;
/**
 * 用户数据实现
 * @author cOde mOnkey
 * 2014年12月2日 下午3:21:11
 */
public interface IUserDao {
	
	public BaseDao getBaseDao();

	/**
	 * 保存
	 * 2014年12月2日 下午3:21:01
	 * @param userPO
	 */
	public void save(OUserPO userPO);
	
	/**
	 * 根据ID查找用户
	 * 2014年12月2日 下午3:21:57
	 * @param userPO
	 * @return
	 */
	public OUserPO findUserById(Long id);
	
	/**
	 * 根据User分装对象查询用户
	 * 2014年12月2日 下午3:23:16
	 * @param hql
	 * @return
	 */
	public List<OUserPO> findUserByUser(OUserPO userPO);
}
