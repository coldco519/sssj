package main.java.login.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import main.java.login.dao.IUserDao;
import main.java.login.entity.OUserPO;
import main.java.util.BaseDao;
import main.java.util.StringUtil;

@Repository
public class UserDaoImpl implements IUserDao{

	@Autowired
	private BaseDao baseDao;
	
	@Override
	public void save(OUserPO userPO) {
		this.baseDao.save(userPO);
	}

	@Override
	public OUserPO findUserById(Long id) {
		return this.baseDao.findEntity(OUserPO.class, id);
	}

	/* (non-Javadoc)
	 * @see main.java.login.dao.IUserDao#findUserByUser(main.java.login.entity.OUserPO)
	 */
	@Override
	public List<OUserPO> findUserByUser(OUserPO userPO) {
		String hql = "from OUserPO u where 1=1";
		if(!StringUtil.isEmpty(userPO.getUsername())){
			hql += " and u.username = '" + userPO.getUsername() + "'";
		}
		if(!StringUtil.isEmpty(userPO.getLocked())){
			hql += " and u.locked = '" +String.valueOf(userPO.getLocked())+ "'";
		}
		return this.baseDao.getResultList(hql);
	}

	@Override
	public BaseDao getBaseDao() {
		return this.baseDao;
	}

	
	
}
