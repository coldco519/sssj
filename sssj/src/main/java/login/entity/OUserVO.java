package main.java.login.entity;

import java.io.Serializable;

import main.java.general.enums.FLAGENUM;

public class OUserVO implements Serializable{

	private static final long serialVersionUID = 1L;

	private String id;
	
	private String username;
	
	private String locked;
	
	
	
	public static OUserVO from(OUserPO po) {
		OUserVO vo = new OUserVO();
		vo.setId(po.getId().toString());
		vo.setUsername(po.getUsername());
		vo.setLocked(FLAGENUM.TRUE.equals(po.getLocked()) ? "����" : "δ����");
		return vo;
	}



	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getUsername() {
		return username;
	}



	public void setUsername(String username) {
		this.username = username;
	}



	public String getLocked() {
		return locked;
	}



	public void setLocked(String locked) {
		this.locked = locked;
	}

}
