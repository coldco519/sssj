package main.java.report.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.java.login.entity.OUserVO;
import main.java.report.OrderDataSource;
import main.java.report.service.IJasperReportService;
import main.java.util.ChartJSUtil;
import main.java.util.PISConstant;

import org.springframework.stereotype.Service;


@Service
public class JasperReportServiceImpl implements IJasperReportService{

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void printUserList(HttpServletRequest request,HttpServletResponse response, List<OUserVO> list) {
		ChartJSUtil.printPDFReport(request, response, new OrderDataSource(list), PISConstant.getProValue("USER_LIST_PATH"), null);
	}

}
