package main.java.report.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.java.login.entity.OUserVO;


public interface IJasperReportService {

	/**
	 * 打印用户列表
	 */
	public void printUserList(HttpServletRequest request,HttpServletResponse response,List<OUserVO> list);
	
}
